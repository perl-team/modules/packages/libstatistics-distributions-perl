Source: libstatistics-distributions-perl
Section: perl
Priority: optional
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Julien Vaubourg <julien@vaubourg.com>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 3.9.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libstatistics-distributions-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libstatistics-distributions-perl.git
Homepage: https://metacpan.org/release/Statistics-Distributions
Testsuite: autopkgtest-pkg-perl

Package: libstatistics-distributions-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}
Multi-Arch: foreign
Description: module for calculating some values of common statistical distributions
 Statistics::Distributions calculates percentage points (5 significant digits)
 of the u (standard normal) distribution, the student's t distribution, the
 chi-square distribution and the F distribution. It can also calculate the upper
 probability (5 significant digits) of the u (standard normal), the
 chi-square, the t and the F distribution. These critical values are needed to
 perform statistical tests, like the u test, the t test, the F test and the
 chi-squared test, and to calculate confidence intervals.
 .
 If you are interested in more precise algorithms you could look at:
 .
 StatLib: http://lib.stat.cmu.edu/apstat/ ;
 .
 Applied Statistics Algorithms by Griffiths, P. and Hill, I.D., Ellis Horwood:
 Chichester (1985)
